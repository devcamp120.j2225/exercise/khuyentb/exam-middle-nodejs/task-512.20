const express = require("expess");

const ConNguoi = require("../Modules/conNguoi");

class HocSinh extends ConNguoi {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, sdt) {
        super (hoTen, ngaySinh, queQuan),
            this.tenTruong = tenTruong,
            this.lop = lop,
            this.sdt = sdt
    }
}

var hocSinhKhuyenTb = new HocSinh("Trinh Bao Khuyen", "20/06/2000", "Long An", "UEH", "KM002", "0333142287");
console.log(hocSinhKhuyenTb);
console.log(hocSinhKhuyenTb instanceof ConNguoi);
console.log(hocSinhKhuyenTb instanceof HocSinh);

module.exports = HocSinh;